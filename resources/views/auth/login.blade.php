
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Iniciar Operacion | CORZA</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.7.2-->
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/login.css">
        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>
    <div id="app">        
        <login-page></login-page>
    </div>
    <!-- <script async type="text/javascript" src="../js/bulma.js"></script> -->
    <script async type="text/javascript" src="/js/app.js"></script>
</body>

</html>
