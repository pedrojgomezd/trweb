<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opelote extends Model
{
	protected $primaryKey = 'ID';
	
    protected $table = 'OPELOTES';
}
