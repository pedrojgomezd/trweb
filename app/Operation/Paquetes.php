<?php 

namespace App\Operation;

use App\Paqueteo;
/**
 * Paquetes
 */
class Paquetes
{

	protected $code;

	protected $session;

	public $paquete;

	public $user;

	public $errors;
	
	function __construct($code, $session, $user)
	{
		$this->code = $code;
		$this->session = $session;
		$this->errors = collect();
		$this->user = $user;
		$this->setPaquete();
	}

	public function setPaquete()
	{
		$paquete = Paqueteo::where('CODIGO', $this->code)->first();

		$this->paquete = $paquete;
	}


	public function verifyErrors()
	{
		if(!$this->existPaquete()){
			return ;
		}

		$this->isUsedPaquete();
	}

	public function existPaquete()
	{
		return $this->paquete ? true : $this->pushError('El paquete no existe', false);
	}

	public function isUsedPaquete()
	{
		return $this->paquete['operario'] 
				? $this->pushError("Paquete cobrado por {$this->paquete->operario->full_name}", true) 
				: false;
	}

	public function pushError($error, $value = true)
	{
		$this->errors->push([$error]);
		return $value;
	}

	public function hasErrors()
	{
		$this->verifyErrors();

		return ($this->errors->count() > 0);
	}

	public function updatePaquete()
	{
		$this->paquete->OPERARIO = $this->user;
        $this->paquete->SESION = $this->session;
        $this->paquete->LECTURA = today();
        $this->paquete->ESTADO = true;
        $this->paquete->save();
	}

	public function data()
	{
		$this->updatePaquete();

		return [
				'paquete' => $this->paquete,
				'paquete["opelote"]' => $this->paquete->opelote,
				'paquete["infolote"]' => $this->paquete->info_lote,
			];
	}
}