<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaqueteoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo' => $this->CODIGO,
            'opelote' => $this->opelote,
            'infolote' => $this->info_lote,
            'operario' => $this->operario
        ];
    }
}
