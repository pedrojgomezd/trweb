<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lectura extends Model
{
    protected $table = 'LECTURA';

    protected $primaryKey = 'PK';

    protected $fillable = ['OPERARIO', 'OPERACION', 'INICIO', 'FIN', 'FECHA', 'Eficiencia'];

    public $timestamps = false;

    public function session()
    {
    	return $this->hasMany(SessionOperacion::class, 'session', 'PK');
    }
}
