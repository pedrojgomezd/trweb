export default {
	setLectura: (state, lectura) => {
		state.lectura = lectura
	},

	newPaquete: (state, paquete) => {
		state.paquete = paquete
	},

	pushPaquete: (state, paquete) => {
		state.paquetes.push(paquete)		
	}
}