<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionOperacion extends Model
{
    protected $table = 'session_operacion';

    protected $fillable = ['operario', 'session', 'paquete','eficiencia', 'time_produced', 'time_working'];

    public $timestamps = false;
}
