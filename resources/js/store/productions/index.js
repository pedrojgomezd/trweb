/*import actions from './actions'
*/
import getters from './getters'
import mutations from './mutations'

const state = {
	lectura: {},
	paquetes: [],
	paquete: {}
}

export default {
	namespaced: true,
	/*actions,
	*/
	state,
	getters,
	mutations	
}