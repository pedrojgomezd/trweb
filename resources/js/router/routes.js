import Production from './../pages/ProductionsPage'
import Login from './../pages/LoginPage'

const routes = [
	{
		path: '/productions/:paquete',
		name: 'Produccion',
		icon: 'si si-cursor',
		component: Production
	},
	{
		path: '/login',
		name: 'login',
		component: Login
	}
]

export default routes