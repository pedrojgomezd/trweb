<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paqueteo extends Model
{
    protected $table = 'PAQUETEO';

    protected $primaryKey = 'CODIGO';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ['LOTE', 'OPERACION', 'PAQUETE', 'LECTURA', 'ESTADO', 'OPERARIO', 'SESION', 'Incentivo', 'Orden', 'Tiempo', 'Fecha', 'Eficiencia', 'IncentivoPaquete', 'EficienciaPaquete', 'EsDudoso', 'ProcesoDudoso'];

    public function operario()
    {
    	return $this->belongsTo(User::class, 'OPERARIO', 'doc');
    }

    public function info_lote()
    {
    	return $this->hasOne(InfoLote::class, 'ID', 'PAQUETE');
    }

    public function opelote()
    {
    	return $this->belongsTo(Opelote::class, 'OPERACION', 'ORDEN');
    }
}
