<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Operation\Operation;

class LoginOperationController extends Controller
{
    public function login(Request $request)
    {
    	$this->validateLogin($request);
    	
    	$operation = new Operation($request);

    	if($operation->hasErrors()) {
    		return response()->json(['errors'=> $operation->errors], 500);
    	}

    	$auth = \Auth::attempt($operation->userCredential());

    	$auth ? $message = $operation->data() : $message = [['status' => 'Alert'], 402];

    	return response()->json($message);

    }

    public function logout()
    {
    	$this->guard()->logout();

        return redirect('/login');
    	return response()->json(['status' => 'logout']);
    }

    public function validateLogin(Request $request)
    {
    	$this->validate($request, [
    			 'doc' => 'required|exists:PERSONAL', 
    			 'code' => 'required'
    			]);

    }

    public function guard()
    {
    	return \Auth::guard();
    }
}
