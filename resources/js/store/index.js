import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import productions from './productions'

Vue.use(Vuex)

const store = new Vuex.Store ({
	modules: {
		user,
		productions
	}
})

export default store