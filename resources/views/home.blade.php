@extends('layouts.app')

@section('content')
<div class="card">
        <nav class="navbar is-dark" role="navigation" aria-label="main navigation">
          <div class="navbar-brand">
            <div class="navbar-item">
              Sesion 542354
            </div>
          </div>

          <div class="navbar-menu">
              <div class="navbar-end">
                  <div class="navbar-item">
                    <div class="field">
                      <p class="control has-icons-left has-icons-right">
                        <input class="input" type="email" placeholder="Email">
                        <span class="icon is-small is-left">
                          <i class="fa fa-barcode"></i>
                        </span>
                      </p>
                    </div>
                  </div>  
              </div>
          </div>
        </nav>
    <div class="card-content">
        <h3 class="subtitle">Informacion de la session</h3>
        <br>
        <nav class="level">
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Tiempo</p>
              <p class="title">3:30:10</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Tiempo improductivo</p>
              <p class="title">5:00:20</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Total unidades</p>
              <p class="title">20</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Incentivos</p>
              <p class="title">$15.000</p>
            </div>
          </div>
        </nav>
        <h3 class="subtitle">Paquete en proceso.</h3>

       <nav class="level">
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Paquete</p>
              <p class="title">P-95G45</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Unidades</p>
              <p class="title">35</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Tiempo</p>
              <p class="title">30 min</p>
            </div>
          </div>
          <div class="level-item has-text-centered">
            <div>
              <p class="heading">Tiempo Restante</p>
              <p class="title">29:20 min</p>
            </div>
          </div>
        </nav>
        <h3 class="subtitle">Eficiencia</h3>
        <progress class="progress is-success" value="90" max="100">90%</progress>

    </div>
</div>
@endsection
