export default {
	paqueteWorking: state => {
		return state.paquete
	},

	totalUnidades: state => {
		let pk = state.paquetes
		return pk.reduce((total, paquete) => {
			return paquete.info_lote.UNIDADES + total
		}, 0)

	},

	getLectura: state => {
		return state.lectura
	},

	tiempoProducido: state => {
		let pk = state.paquetes

		return pk.reduce((total, paquete) => {
			return (paquete.opelote.ESTANDAR * paquete.info_lote.UNIDADES) + total
		}, 0)
	},

	duracionDelPaquete: state => {
		return ((60 * state.paquete.opelote.ESTANDAR) * state.paquete.info_lote.UNIDADES) / 60
	}
}