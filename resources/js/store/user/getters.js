export default {
	fullName: state => {
		return `${state.user.nombres} ${state.user.apellidos}`
	},

	document: state => {
		return state.user.doc
	}
}