<?php 

namespace App\Operation;

use Illuminate\Http\Request;
use App\User;
use App\Paqueteo;
use App\Lectura;
use App\Http\Resources\PaqueteoResource;
/**
 * Control de operaciones y paquetes
 */
class Operation
{
	public $request;

	public $user;

	public $paquete;

	public $errors;

	public $lectura;
	
	function __construct(Request $request)
	{
		$this->request = $request;
		$this->paquete = collect();
		$this->errors = collect();
		$this->setUser();
		$this->setPaquete();
	}

	public function setUser()
	{
		$this->user = User::where('doc', $this->request['doc'])->first();
	}

	public function setPaquete()
	{
		$this->paquete = Paqueteo::where('CODIGO', $this->request->code)->first();
		// $this->paquete = new PaqueteoResource($paquete);
	}

	public function verifyPaquete()
	{
		if (!$this->existsPaquete()) {
			return;
		}
		
		$this->isUsedPaquete();

	}

	public function isUsedPaquete()
	{
		return $this->paquete['operario'] 
				? $this->pushError("Paquete cobrado por {$this->paquete->operario->full_name}", true) 
				: false;
	}

	public function existsPaquete()
	{
		return $this->paquete ? true : $this->pushError('El paquete no existe', false);
	}

	public function updatePaquete()
	{
		$this->paquete->OPERARIO = $this->user->doc;
        $this->paquete->SESION = $this->lectura->PK;
        $this->paquete->LECTURA = today();
        $this->paquete->ESTADO = true;
        $this->paquete->save();
	}

	public function pushError($error, $value = true)
	{
		$this->errors->push([$error]);
		return $value;
	}

	public function hasErrors()
	{
		$this->verifyPaquete();
		
		return ($this->errors->count() > 0);
	}

	public function createLectura()
	{
		$this->lectura = Lectura::create([
			'OPERARIO' => $this->user->doc, 
			'OPERACION' => 0, 
			'INICIO' => now()->format('h:m:s'), 
			'FECHA' => today() 
		]);
	}

	public function userCredential()
	{
		return ['doc' => $this->user->doc, 'password' => $this->user->doc];
	}
	public function data()
	{
		$this->createLectura();
		$this->updatePaquete();
		
		return [
				'user' => $this->user, 
				'paquete' => $this->paquete,
				'paquete["opelote"]' => $this->paquete->opelote,
				'paquete["infolote"]' => $this->paquete->info_lote,
				'lectura' => $this->lectura->load('session')
			];
	}

}