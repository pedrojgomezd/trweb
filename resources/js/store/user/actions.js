export default {
	loadUser: (context) => {
		return new Promise((resolver, reject) => {
			axios.post('/user_login')
				.then(({data}) => {
					console.log('Hi', data)
					context.commit('setUser', data)
					resolver(data)
				})
				.catch(() => {
					reject('Error en la carga del usuario')
				})
		})
	}
}