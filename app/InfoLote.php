<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoLote extends Model
{
    protected $table = 'INFOLOTES';

    protected $casts = [ 'UNIDADES'=> 'integer'];

}
