<?php

use Illuminate\Http\Request;
use App\Operation\Paquetes;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test', function(){
	return 'rest';
});

Route::post('productions/new_code/{code}/{session}/{user}', function($code, $session, $user) {
	$newPaquete = new Paquetes($code, $session, $user);

	if($newPaquete->hasErrors()){
		return response()->json($newPaquete->errors, 500);
	}

	return response()->json($newPaquete->data());
});