
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - Free Bulma template</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.7.2-->
    <link rel="stylesheet" href="/css/app.css" />
    <link rel="stylesheet" type="text/css" href="/css/login.css">
</head>

<body>
    <section class="hero is-success is-fullheight">
        <div class="hero-body">
            <div class="container has-text-centered">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-grey">Comenzar operacion</h3>
                    <p class="subtitle has-text-grey">Ingrese credenciales.</p>
                    <div class="box">
                        <form>
                            <div class="field">
                                <div class="control">
                                    <input class="input is-large" type="text" placeholder="Documento" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input class="input is-large" type="text" placeholder="Paquete">
                                </div>
                            </div>
                            <button class="button is-block is-info is-large is-fullwidth">Entrar</button>
                        </form>
                    </div>
                    <p class="has-text-grey">
                        <a href="../">Necesita ayuda?</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- <script async type="text/javascript" src="../js/bulma.js"></script> -->
</body>

</html>
